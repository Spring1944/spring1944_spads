# Player Announce -- SPADS plugin to announce player join/leave to #s44games
#
# Copyright (C) 2015 Fedja Beader
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# We define our plugin class
package PlayerAnnounce;

# We use strict Perl syntax for cleaner code
use strict;

# We use the SPADS plugin API module
use SpadsPluginApi;

# We don't want warnings when the plugin is reloaded
no warnings 'redefine';

# This is the first version of the plugin
my $pluginVersion='1.0';

# This plugin is compatible with any SPADS version which supports plugins
# (only SPADS versions >= 0.11.5 support plugins)
my $requiredSpadsVersion='0.11.5';

# This is how SPADS gets our version number (mandatory callback)
sub getVersion { return $pluginVersion; }

# This is how SPADS determines if the plugin is compatible (mandatory callback)
sub getRequiredSpadsVersion { return $requiredSpadsVersion; }




my $hostedBattleID;
my $clientCount=0;
my $masterChannel="";


# This is our constructor, called when the plugin is loaded by SPADS (mandatory callback)
sub new
{
	# Constructors take the class name as first parameter
	my $class=shift;

	# We create a hash which will contain the plugin data
	my $self = {};
	# We instanciate this hash as an object of the given class
	bless($self,$class);

	onLobbyConnected() if(getLobbyState() > 3);
	slog("Plugin loaded (version $pluginVersion)",3);

	onLobbyConnected();

	# We return the instantiated plugin
	return $self;
}



# This callback is called each time we (re)connect to the lobby server
sub onLobbyConnected
{
	# When we are disconnected from the lobby server, all lobby command
	# handlers are automatically removed, so we (re)set up our command
	# handler here.
	addLobbyCommandHandler({JOINEDBATTLE => \&hLobbyJoinedBattle});
	addLobbyCommandHandler({LEFTBATTLE => \&hLobbyLeftBattle});
	addLobbyCommandHandler({OPENBATTLE => \&hLobbyOpenBattle});
}

# This callback is called when the plugin is unloaded
sub onUnload
{
	# We remove our lobby command handler when the plugin is unloaded
	removeLobbyCommandHandler(['JOINEDBATTLE']);
	removeLobbyCommandHandler(['LEFTBATTLE']);
	removeLobbyCommandHandler(['OPENBATTLE']);
}

sub hLobbyOpenBattle
{
	# $command is OPENBATTLE
	# $battleID is the ID we want
	my ($command,$bid)=@_;
	$hostedBattleID=$bid;
	$clientCount = 0;
	$masterChannel = getSpadsConf()->{masterChannel};
	sayChan($masterChannel, "Announce plugin loaded, my battle ID is $hostedBattleID.");
}

# This is the handler we set up on JOINEDBATTLE lobby command.
# It is called each time a player joins the battle lobby?
sub hLobbyJoinedBattle
{
	# $command is the lobby command name (JOINEDBATTLE)
	# $battleID is the battle ID, huh?
	# $userName is the name of the user
	# $scriptPassword is the password
	my ($command,$battleID,$userName,$scriptPassword)=@_;

	# First we check it is not SPADS itself
#	my $p_spadsConf=getSpadsConf();
#	return if($userName eq $p_spadsConf->{lobbyLogin});

	if($hostedBattleID && $battleID == $hostedBattleID) {
		$clientCount = $clientCount + 1;
		#sayPrivate("ThinkIRC", "#clients: $clientCount, joined: $userName");
		sayChan($masterChannel, "#clients: $clientCount, joined: $userName");
	}
#	} else {
#		sayPrivate("ThinkIRC", "joined some other room: $userName, $battleID");
#	}
}

sub hLobbyLeftBattle
{
	# $command is the lobby command name (LEFTBATTLE)
	# $battleID is the battle ID, huh?
	# $userName is the name of the user
	my ($command,$battleID,$userName)=@_;

	if($hostedBattleID && $battleID == $hostedBattleID) {
		$clientCount = $clientCount - 1;
		#sayPrivate("ThinkIRC", "#clients: $clientCount, left: $userName");
		sayChan($masterChannel, "#clients: $clientCount, left: $userName");
	}
}

1;
