separate;



function get_lobby_password (ahname : in string) return string is
begin
   case ahname is
   when "WorldAtWarII"         => return "password";

   when others                 => raise NoEntryException;
   end case;
end get_lobby_password;


replays_account  : constant string := "WorldAtWarII_replay2";
replays_password : constant string := "password";
