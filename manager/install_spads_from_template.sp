separate;
-- SPADS manager -- subprogram for installing SPADS from a template install
-- Copyright (C) 2015-2018 Fedja Beader
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

procedure install_spads_from_template (
	spring_engine_dir          : string;
	spads_template_install_dir : string;
	spads_install_dir          : string;
	spring_unitsync_path       : string;
	spring_data_path           : string)
is
	current_dir : constant string := PWD;
begin
	test -d "$spads_install_dir"; -- already installed?
	if $? = 0 then return; end if;


	-- Oh darn, we have to install SPADS for this engine :/
	put_line ("Installing SPADS from template for engine: " & spring_engine_dir);


	test -d "$spads_template_install_dir";
	if $? /= 0 then raise FileSystemException; end if; -- TODO: Install SPADS into template dir if missing



	-- First update template install
	cd "$spads_template_install_dir";
	if $? /= 0 then raise FileSystemException; end if; -- Failed to change directory to SPADS install template

	./update.pl stable -a;
	-- die 1 "spads ./update.pl failed!\n"
	if $? /= 0 then raise GeneralErrorException; end if; -- SPADS update failed

	cd "$current_dir";
	if $? /= 0 then raise FileSystemException; end if;



	-- --reflink is for btrfs CoW copies
	cp "--archive" "--recursive" "--verbose" "--reflink=auto" "$spads_template_install_dir" "$spads_install_dir";
	if $? /= 0 then raise FileSystemException; end if; -- Copying template install failed


	cd "$spads_install_dir";
	if $? /= 0 then raise FileSystemException; end if; -- Failed to change directory for regenerating libunitsync bindings

	-- Regenerate unitsync bindings
	spads_installer_stdin : string :=
	    spring_unitsync_path & ASCII.LF
	  & spring_data_path & ASCII.LF
	  & "none" & ASCII.LF;

	printf "%s" "$spads_installer_stdin" | ./spadsInstaller.pl -g;
	if $? /= 0 then raise GeneralErrorException; end if; -- libunitsync binding regeneration failed

	cd "$current_dir";
	if $? /= 0 then raise FileSystemException; end if;

end install_spads_from_template;
