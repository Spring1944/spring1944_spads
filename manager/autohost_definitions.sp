separate;
-- SPADS manager -- Autohost definitions - configures each host
-- Copyright (C) 2015-2018 Fedja Beader
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

function merge_strings_with_semicolon (lhs : in string; rhs : in string) return string is
begin
	if strings.length (lhs) = 0 then
		return rhs;
	elsif strings.length (rhs) = 0 then
		return lhs;
	else
		return lhs & ";" & rhs;
	end if;
end merge_strings_with_semicolon;


function get_autoload_plugins (ahname : in string) return string is
	-- plugin lists must be ; separated
	global_plugins     : constant string := "PlayerAnnounce";

	S44Update          : constant string := "S44Update";
	JALUpdate          : constant string := "JALUpdate";
	TCUpdate           : constant string := "TCUpdate";

	plugins            : string;
begin
	-- order in plugins is least general to most general plugin
	case ahname is
	when "CursedBot_1"          => plugins := TCUpdate;
	when "WorldAtWarII_rapid"   => plugins := S44Update;
--	when "WorldAtWarII_rapid1"  => plugins := S44Update;
	when "WorldAtWarII_rapid2"  => plugins := S44Update;
	when "WorldAtWarII_rapid3"  => plugins := S44Update;
	when "WorldAtWarII_engine"  => plugins := S44Update;
	when "WorldAtWarII_engine1" => plugins := S44Update;
--	when "WorldAtWarII_test3"   => plugins := JALUpdate;
	when others                 => plugins := "";
	end case;

	if strings.glob ("*WorldAtWar*", ahname) then
		plugins := merge_strings_with_semicolon ("FirstWelcomeMsg", plugins);
	end if;

	return merge_strings_with_semicolon (global_plugins, plugins);
end get_autoload_plugins;



function get_battle_name (ahname : in string) return string is
begin
	case ahname is
	when "CursedBot_1"          => return "The Cursed stable (1)";

	when "WAWII_equilibrio1"    => return "S:1944 Equilibrio (get S:1944 2.0 first) [1]";
	when "WAWII_equilibrio2"    => return "S:1944 Equilibrio (get S:1944 2.0 first) [2]";

	when "WorldAtWarII"         => return "S:1944 Stable! (World War Two game) [1]";
	when "WorldAtWarII_1"       => return "S:1944 Stable! (World War Two game) [1]";
	when "WorldAtWarII_2"       => return "S:1944 Stable! (World War Two game) [2]";
	when "WorldAtWarII_3"       => return "S:1944 Stable! (World War Two game) [3]";
	when "WorldAtWarII_4"       => return "S:1944 Stable! (World War Two game) (4)";
	when "WorldAtWarII_5"       => return "S:1944 Infantry-only!";

	when "WorldAtWarII_engine"  => return "S:1944 ***The Engine Room***";
	when "WorldAtWarII_engine1" => return "S:1944 Engine testing (probably broken)";

	when "WorldAtWarII_git"     => return "S:1944 development (git)";

	when "WorldAtWarII_legacy1" => return "S:1944 Legacy room for slow computers";

	when "WorldAtWarII_rapid"   => return "S:1944 development (1) & Tools->JoinChannel->s44";
	when "WorldAtWarII_rapid1"  => return "S:1944 Stable (with Hungary&Sweden)";
	when "WorldAtWarII_rapid2"  => return "S:1944 public development (2) & Tools->JoinChannel->s44";
	when "WorldAtWarII_rapid3"  => return "S:1944 crazy games host/unlimited metal!";

	when "WorldAtWarII_test"    => return "S:1944  test";
	when "WorldAtWarII_test2"   => return "S:1944  Infantry only";
	when "WorldAtWarII_test3"   => return "S:1944 ***JAL's mutator*** (download 2.0 first)";
	when others                 => raise NoEntryException;
	end case;
end get_battle_name;



function get_battle_password (ahname : in string) return string is
	devel_pass : constant string := "omegatest";
begin
	case ahname is
	when "WAWII_equilibrio2"    => return devel_pass;
	when "WorldAtWarII_engine1" => return devel_pass;
	when "WorldAtWarII_engine2" => return devel_pass;
	when others                 => return "*"; -- no password
	end case;
end get_battle_password;



function get_battle_preset (ahname : in string) return string is
begin
	case ahname is
	when "WorldAtWarII_rapid3"  => return "crazy";
	when "WorldAtWarII_test2"   => return "infantry";
	when others                 => return "default";
	end case;
end get_battle_preset;



-- This is a local thing, no port forwarding
function get_control_port (ahname : in string) return natural is
begin
	case ahname is
	when "CursedBot_1"          => return 8501;

	when "WAWII_equilibrio1"    => return 8441;
	when "WAWII_equilibrio2"    => return 8442;

	when "WorldAtWarII"         => return 8401;
	when "WorldAtWarII_1"       => return 8401;
	when "WorldAtWarII_2"       => return 8402;
	when "WorldAtWarII_3"       => return 8403;
	when "WorldAtWarII_4"       => return 8404;
	when "WorldAtWarII_5"       => return 8405;

	when "WorldAtWarII_engine"  => return 8421;
	when "WorldAtWarII_engine1" => return 8421;
	when "WorldAtWarII_engine2" => return 8422;

	when "WorldAtWarII_git"     => return 8454;

	when "WorldAtWarII_legacy1" => return 8461;

	when "WorldAtWarII_rapid"   => return 8411;
	when "WorldAtWarII_rapid1"  => return 8411;
	when "WorldAtWarII_rapid2"  => return 8412;
	when "WorldAtWarII_rapid3"  => return 8413;

	when "WorldAtWarII_test"    => return 8431;
	when "WorldAtWarII_test1"   => return 8431;
	when "WorldAtWarII_test2"   => return 8432;
	when "WorldAtWarII_test3"   => return 8433;
	when "WorldAtWarII_test4"   => return 8434;
	when "WorldAtWarII_test5"   => return 8435;

	when others                 => raise NoEntryException;
	end case;
end get_control_port;



function get_end_game_arguments (ahname : in string) return string is
	end_game_arguments : constant string := ""
	  &  "--startTimestamp " & '"' & "%startTimestamp" & '"'
	  & " --endTimestamp " & '"' & "%endTimestamp" & '"'
	  & " --duration " & '"' & "%duration" & '"'
	  & " --startPlayingTimestamp " & '"' & "%startPlayingTimestamp" & '"'
	  & " --endPlayingTimestamp " & '"' & "%endPlayingTimestamp" & '"'
	  & " --gameDuration " & '"' & "%gameDuration" & '"'
	  & " --engineVersion " & '"' & "%engineVersion" & '"'
	  & " --mod " & '"' & "%mod" & '"'
	  & " --map " & '"' & "%map" & '"'
	  & " --type " & '"' & "%type" & '"'
	  & " --structure " & '"' & "%structure" & '"'
	  & " --nbBots " & '"' & "%nbBots" & '"'
	  & " --ahName " & '"' & "%ahName" & '"'
	  & " --ahAccountId " & '"' & "%ahAccountId" & '"'
	  & " --ahPassword " & '"' & "%ahPassword" & '"'
	  & " --ahPassHash " & '"' & "%ahPassHash" & '"'
	  & " --demoFile " & '"' & "%demoFile" & '"'
	  & " --gameId " & '"' & "%gameId" & '"'
	  & " --result " & '"' & "%result" & '"'
	  & " --cheating " & '"' & "%cheating" & '"';
begin
	-- replay uploading yes(1)/no(0)
	case ahname is
	when "CursedBot_1"          => return end_game_arguments & " --upload 1";
	when "WorldAtWarII"         => return end_game_arguments & " --upload 1";
	when "WorldAtWarII_2"       => return end_game_arguments & " --upload 1";
	when "WorldAtWarII_3"       => return end_game_arguments & " --upload 1";
	when "WorldAtWarII_4"       => return end_game_arguments & " --upload 1";
	when "WorldAtWarII_legacy1" => return end_game_arguments & " --upload 1";
	when others                 => return end_game_arguments & " --upload 0";
	end case;
end get_end_game_arguments;



function get_engine_version (ahname : in string) return string is
	stable          : constant string := "103.0";
	testing         : constant string := "104.0";
begin
	case ahname is
	when "WorldAtWarII_engine"  => return testing;
	when "WorldAtWarII_engine1" => return testing;
	when "WorldAtWarII_legacy1" => return "100.0";
	when "CursedBot_1"          => return testing;
	when others                 => return stable;
	end case;
end get_engine_version;



function get_engine_type (ahname : in string) return string is
begin
	case ahname is
	when "WorldAtWarII_3"       => return "headless";
	when others                 => return "dedicated";
	end case;
end get_engine_type;



function get_game (ahname : in string) return string is
	cursed_stable_100 : constant string := "The Cursed 1.400";--"~The\ Cursed\ \d+\.\d+";
	cursed_stable_103 : constant string := "~The\ Cursed\ 1\.(\S+)";
	s44_old_stable : constant string := "~Spring\:\ 1944\ v2\.0";
	s44_stable     : constant string := "~Spring\:\ 1944\ 3\.(\S+)";
	s44_rapid      : constant string := "~Spring\:\ 1944\ test(\S+)";
	s44_pre_102    : constant string := "~Spring\:\ 1944\ test(\S+)5a39a18";
--	s44_pre_crash  : constant string := "~Spring\:\ 1944\ test(\S+)26650d4";
	s44_git        : constant string := "~Spring\:\ 1944\ \$VERSION";
	s44_equilibrio : constant string := "~Spring\:\ 1944\ equilibrio\ 2\.(\S+)";
begin
	case ahname is
	when "CursedBot_1"          => return cursed_stable_103;
	-- override, seems lobby is broken with downloading later ones
--	when "WAWII_equilibrio1"    => return "~Spring\:\ 1944\ equilibrio\ 2\.0982";
	when "WAWII_equilibrio1"    => return s44_equilibrio;
	when "WAWII_equilibrio2"    => return s44_equilibrio;

	when "WorldAtWarII"         => return s44_stable;
	when "WorldAtWarII_2"       => return s44_stable;
	when "WorldAtWarII_3"       => return s44_stable;

	when "WorldAtWarII_engine"  => return s44_rapid;
	when "WorldAtWarII_engine1" => return s44_rapid;
	when "WorldAtWarII_engine2" => return s44_equilibrio;

	when "WorldAtWarII_git"     => return s44_git;

	when "WorldAtWarII_legacy1" => return s44_old_stable;

	when "WorldAtWarII_rapid"   => return s44_rapid;
	when "WorldAtWarII_rapid1"  => return s44_pre_102;
	when "WorldAtWarII_rapid2"  => return s44_rapid;
	when "WorldAtWarII_rapid3"  => return s44_rapid;

	when "WorldAtWarII_test3"   => return s44_equilibrio;

	when others                 => return s44_stable;
	end case;
end get_game;



function get_map_list (ahname : in string) return string is
begin
   case ahname is
   when "CursedBot_1"          => return "cursed";
   when "WorldAtWarII_rapid2"  => return "naval";
   when others                 => return "small";
   end case;
end get_map_list;



function get_master_channel (ahname : in string) return string is
begin
   if strings.glob ("*Cursed*", ahname) then
      return "TheCursed";
   else
      return "s44games";
   end if;
end get_master_channel;



function get_port (ahname : in string) return natural is
begin
	case ahname is
	when "CursedBot_1"          => return 9101;

	when "WAWII_equilibrio1"    => return 9041;
	when "WAWII_equilibrio2"    => return 9042;

	when "WorldAtWarII"         => return 9001; -- conflict
	when "WorldAtWarII_1"       => return 9001;
	when "WorldAtWarII_2"       => return 9002;
	when "WorldAtWarII_3"       => return 9003;
	when "WorldAtWarII_4"       => return 9004;
	when "WorldAtWarII_5"       => return 9005;

	when "WorldAtWarII_engine"  => return 9011;
	when "WorldAtWarII_engine1" => return 9011;
	when "WorldAtWarII_engine2" => return 9012;

	when "WorldAtWarII_git"     => return 9005;

	when "WorldAtWarII_legacy1" => return 9005;

	when "WorldAtWarII_rapid"   => return 9021;
	when "WorldAtWarII_rapid1"  => return 9021;
	when "WorldAtWarII_rapid2"  => return 9022;
	when "WorldAtWarII_rapid3"  => return 9023;

	when "WorldAtWarII_test"    => return 9031;
	when "WorldAtWarII_test1"   => return 9031;
	when "WorldAtWarII_test2"   => return 9032;
	when "WorldAtWarII_test3"   => return 9033;
	when "WorldAtWarII_test4"   => return 9034;
	when "WorldAtWarII_test5"   => return 9035;
	when others                 => raise NoEntryException;
	end case;
end get_port;



function get_promote_channels (ahname : in string) return string is
   list : string := get_master_channel (ahname);
begin
   if not strings.glob ("*Cursed*", ahname) then
      list := merge_strings_with_semicolon (list, "s44");
--      list := strings.insert (list, strings.length (list) + 1, ";s44");
   end if;

   if get_battle_password (ahname) = "*" then
      list := merge_strings_with_semicolon (list, "main");
--      return list & ";main";
   end if;

   return list;
end get_promote_channels;



function get_welcome_message (ahname : in string) return string is
	welcome_header : constant string := ""
	  & "Hi %u (%d), welcome to %n (SPADS %v, automated host)."
	  & "|!For help, say '!help'. Map link: %a"
	  & "|!Replays (demos) are available at: https://think.nsupdate.info/s44/autohosts/demos";

	s44_message : constant string := ""
	  & "|!tutorial: https://think.nsupdate.info/s44/tutorial"
	  & "|!wiki: http://spring1944.net/wiki/index.php?title=Main_Page"
	  & "|!Channels: #s44 (s44 discussions), #s44games (join/leave announce)";

	cursed_message : constant string := ""
	  & "Forum: https://springrts.com/phpbb/viewforum.php?f=54"
	  & "|!Channels: #TheCursed (discussions and join/leave announce)";
begin
	case ahname is
	when "CursedBot_1"          => return welcome_header & cursed_message;
	when others                 => return welcome_header & s44_message;
	end case;
end get_welcome_message;



function get_welcome_message_in_game (ahname : in string) return string is
	welcome_message_in_game : constant string := ""
	  & "|!A game is in progress since %t."
	  & "|!Say '!status' for more information, and '!notify' to be notified when this game ends.";
begin
	return get_welcome_message (ahname) & welcome_message_in_game;
end get_welcome_message_in_game;
